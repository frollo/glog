Slot di inventario
I PC hanno un numero di slot di inventario pari alla loro forza.
Solo gli articoli trasportati valgono per questo limite. Cavalli, mercenari e
i carrelli non contano. Abbigliamento, gioielli minori non magici e
altri articoli cosmetici non occupano slot di inventario a meno che non lo facciano
sono particolarmente voluminosi.
Uno slot di inventario può contenere:
• 3 armi leggere (pugnali)
• 1 arma media (spada)
• 0,5 Armi pesanti (martelli)
• 1 arco
• 20 frecce
• 3 bottiglie o boccette
• 1 lanterna
• 3 libri
• 3 Razioni
I primi 3 slot di inventario sono slot a disegno rapido e possono essere
accessibile in qualsiasi momento. Ci vuole 1 round per recuperare un oggetto
qualsiasi altra slot di inventario.
La gestione dell'inventario è fondamentale. Selezione e tracciamento
gli strumenti per la risoluzione dei problemi sono abilità dei giocatori.
Ingombro
Qualsiasi oggetto in eccesso rispetto alle slot di inventario di un personaggio (Forza)
guadagna 1 punto di ingombro per slot. Ogni punto di
L'ingombro impone una penalità di -1 a Stealth, Movement e
Difesa.
Con 6 o più punti di Encumbrance, muoversi rapidamente è
impossibile. Con 10 o più punti di Encumbrance, in movimento
oltre un gattonare è estenuante.
Fatica
Sprint ripetutamente, viaggiare all'aperto senza sosta o eccessivamente
attività stancanti in affaticamento. Ogni livello di fatica richiede 1
Spazio pubblicitario. Di solito viene rimosso riposando.
Armatura
• L'armatura di cuoio occupa 0 slot di inventario e fornisce +2
alla difesa.
• L'armatura a catena occupa 2 slot di inventario e fornisce un +4
bonus alla difesa. Implica anche una penalità di -2 a
• Movimento e azione furtiva.
• L'armatura a piastre occupa 4 slot inventario e fornisce un +6
bonus alla difesa. Implica anche una penalità di -4 a
Movimento e azione furtiva.
• Gli scudi occupano 1 slot di inventario e forniscono un bonus di +1 a
Difesa. Un PC può dividere il proprio scudo per ridurre l'arrivo
danno di 1d12. Lo scudo viene successivamente rotto.
Armi da mischia
• Le armi leggere (pugnali) occupano 1/3 di una slot di inventario
e infligge 1d6 + danni bonus alla Forza in mischia.
• Le armi medie (spade) occupano 1 slot di inventario e
infligge 1d8 + danni bonus alla Forza se esercitato in una mano
o 1d10 + danni bonus alla Forza se esercitati con entrambe le mani.
• Le armi pesanti (martelli) infliggono 1d12 + bonus alla forza
danno ma deve essere maneggiato con entrambe le mani.
Armi a distanza
Le armi a distanza ottengono -1 in attacco per ogni 10 "oltre il loro
intervallo elencato.
• Le armi leggere (pugnali) lanciate prendono 1/3 di un
Scorta dell'inventario e infligge 1d6 danni e ha un raggio di 20 '.
• Le imbragature occupano 1 slot di inventario. Infliggono 1d6 danni e
avere un raggio di 20 '.
• Gli archi occupano 1 slot di inventario. Infliggono 1d6 danni e
avere un intervallo di 30 ".
• Le balestre occupano 1 slot di inventario. Trattano 1d12
danno e ha un raggio di 30 '. Una balestra richiede 1 round per
ricaricare.
Sia gli archi che le balestre usano le frecce. Uno slot di inventario può
contiene 20 frecce. Dopo il combattimento, le frecce possono essere recuperate. 50%
sarà utilizzabile.
Sorgenti luminose
Al di fuori della gamma di luce elencata, le fonti luminose si illuminano
ombre. Forme vaghe, movimento e superfici riflettenti lo faranno
essere visibile nelle ombre, ma nessun dettaglio può essere distinto.
Durata del raggio della sorgente luminosa
Torcia 20 "luce, 20" ombre 1 ora
Lanterna 30 'di luce, 30' di ombre 3 ore / ora di olio.
Candela 5 "luce, 10" ombre 1 ora
CC BY-NC-SA
5
Inventario e articoli
L'accensione di una torcia o lanterna richiede 1 round. Si presume che i PC lo siano
con equipaggiamento base di avviamento. Magia o trucioli possono incendiarsi a
fonte di luce immediatamente.
Seguaci
Assunto a un tasso fisso (vedere il listino prezzi). Il lavoro pericoloso può
richiedono bonus. I noleggi generici hanno 10 in tutte le statistiche, 5 HP e
nessuna competenza al di fuori della loro professione.
I PC possono avere fino a 2 + i loro noleggi bonus Charisma senza
eventuali problemi. I personaggi della Seconda Proprietà possono avere fino a
6 + il loro bonus di carisma. Tuttavia, è possibile acquisire noleggi extra
può essere sleale, astuto o di scarsa qualità.
Al di sopra di tutti gli elementi richiesti per il loro lavoro, un mercenario può trasportare 3
Spazio inventario di articoli per PC.