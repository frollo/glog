# Regole base

## Indicazioni per il GM

Chiedi di effettuare un tiro solo quando sia il successo che il fallimento sono interessanti. Se il fallimento è inevitabile per via di quello che sta succedendo in gioco, non tirare; un PG non può fare una prova di Forza per vedere se sbattendo le braccia riesce ad arrivare sulla Luna. Se il fallimento è noioso, non tirare; se un PG non riesce ad aprire un banale lucchetto e ha tutto il tempo del mondo, continuerà a ritirare finché non gli riesce. Limitati a farglielo aprire o ad applicare una penalità sul tempo. Se il fatto che un PG non veda qualcosa vuol dire che non la vedrà mai, digli semplicemente quello che vede. Non serve tirare.

I tiri falliti hanno delle conseguenze. La più ovvia è il tempo: perdere tempo significa che ci sono più possibilità per le cose brutte di accadere ai PG.

Tira il meno possibile. Siccome le caratteristiche sono generate e assegnate casualmente, due personaggi nello stesso gruppo potrebbero avere possibilità estremamente diverse di riuscire in uno stesso compito. Minimizza questo rischio usando Attacco e il Tiro Salvezza (che sono gli stessi per la maggior parte dei PG dello stesso livello) e successi e fallimenti automatici.

Dal punto di vista del giocatore, ogni tiro è un rischio. Fare una prova di Forza per saltare oltre un burrone è rischioso, metterci una scala di traverso e muoversi su quella è molto meno rischioso e non richiede di tirare. Combattere è rischioso, mentre evitare del tutto il combattimento potrebbe non richiedere tiri.

## La meccanica centrale

Quasi tutto è basato sul tirare un d20. Se il risultato è **uguale o inferiore** al tuo obiettivo, hai successo, altrimenti fallisci.

In tutto questo documento ogni volta che trovate "Tira sotto..." vuol dire "Tira uguale o sotto...", è solo troppo faticoso scrivere tutta quella roba ogni volta.

## Abilità

Un'Abilità è una parola che descrive qualcosa che il personaggio sa fare. Non ci sono regole specifiche per usarle. Potresti richiedere un tiro sotto Intelligenza o qualsiasi altra statistica, ma, in generale, lascia che il personaggio con l'abilità faccia quello che l'abilità descrive.

## Statistiche

| Statistica | Bonus |
| ---------- | ----- |
| 1 | -3 |
| 2 | -3 |
| 3 | -2 |
| 4 | -2 |
| 5 | -2 |
| 6 | -1 |
| 7 | -1 |
| 8 | -1 |
| 9 | 0 |
| 10 | 0 |
| 11 | 0 |
| 12 | +1 |
| 13 | +1 |
| 14 | +1 |
| 15 | +2 |
| 16 | +2 |
| 17 | +2 |
| 18 | +3 |
| 19 | +3 |
| 20 | +3 |
| 21 | +4 |
| 22 | +4 |
| 23 | +4 |
| 24 | +5 |

I personaggi hanno 6 statistiche. Ogni statistica ha un "bonus" che è una specie di versione condensata della statistica. Non sommare il bonus ai tiri.

**Forza**: Tira sotto Forza per aprire porte, spezzare catene o scalare un picco. Il numero di oggetti che un personaggio può portare è determinato dal suo bonus di Forza, così come il danno che infligge nel combattimento corpo a corpo.

**Destrezza**: Tira sotto Destrezza per schivare trappole, saltare oltre ostacoli o compiere operazioni difficili sotto pressione. La Difesa, il Movimento e la Furtività di un personaggio sono tutte influenzate dal suo bonus di Destrezza.

**Costituzione**: Tira sotto Costituzione per resistere a freddo, malattia, fatica, annegamento o veleno. I Punti Vita di un personaggio sono basati sulla sua Costituzione.

**Intelligenza**: Tira sotto Intelligenza per risolvere puzzle difficili, leggere testi oscuri o identificare oggetti e tesori.

**Saggezza**: Tira sotto Saggezza per riconoscere bugie, individuare pericoli nascosti o resistere a incantesimi che alterano la mente. I personaggi, inoltre, useranno Saggezza per determinare l'ordine di iniziativa.

**Carisma**: Tira sotto Carisma per lusingare, raggirare o trattare. Il Tiro Salvezza di un personaggio è influenzato dal suo bonus di Carisma.

### Numeri associati

**Attacco**: Comincia a 11, aumenta col livello. Tira sotto Attacco, opposto dalla difesa del tuo avversario, per colpire.

**Difesa**: 10 + il bonus di Destrezza **o** quello dell'armatura.

**Movimento**: 12 + il bonus di Destrezza. Determina quanto velocemente si può muovere un PG. Penalizzato dall'armatura.

**Furtività**: 5 + il bonus di Destrezza. Viene usato in opposizione al tiro sotto Saggezza di un avversario per vedere se il personaggio viene notato. Penalizzato dall'armatura.

**Tiro Salvezza**: 5 + il bonus di Carisma. Aumenta col livello. Se un effetto, attacco o sfida non ricade sotto nessun'altra caratteristica o tiro, usa il Tiro Salvezza. Il Tiro Salvezza rappresenta la fortuna di un personaggio e la sua determinazione a resistere alla crudele mano del fato.

| Tipo di armatura | Bonus | Malus |
| ---------------- | ----- | ----- |
| Armatura di cuoio | +2 | 0 |
| Cotta di maglia | +4 | -2 |
| Armatura a piastre | +6 | -4 |

