1. Scegli o tira per la tua razza. La razza del tuo personaggio garantirà
loro un vantaggio, uno svantaggio e una statistica che possono ripetere.
2. Rotola le tue statistiche. 3d6 in ordine. È possibile rileggere lo Stat fornito da
la tua razza e scegli il risultato più alto.
3. Scegli o lancia per la tua classe. Ottieni il primo modello (A) e
qualsiasi altro oggetto e bonus.
4. Tira per qualsiasi abilità o oggetto richiesto dalla tua classe.
5. Guadagna 1d10cp. Se l'intero gruppo è di livello 1, anche tutti i PC ottengono a
coperta (1 slot) e 3 razioni (1 slot).

# L'avventuriero di base

È previsto un PC appena creato senza modelli di classe essere ragionevolmente competente nella maggior parte dei settori ma non specializzato in qualsiasi direzione. Il GLOG non riguarda le classi. Le lezioni sono solo una serie di strumenti extra per fare le cose. Le lezioni sono significative meno importante di quello che fai intorno al tavolo.

# Classi ultra-core
Le classi di base, equilibrate e del tutto sensate. Adatto a nuovi giocatori, giocatori incerti o piccoli gruppi.
    * Combattente, ladro e mago (ortodosso).

# Classi di base

Le nove classi elencate qui sono state testate in più combinazioni e funzioni ragionevolmente bene.
    * Combattente, Barbaro, Cavaliere.
    * Ladro, cacciatore, monaco.
    * Mago, Stregone, Evocatore.

# Classi di novità
Le classi qui elencate richiedono stili di gioco insoliti, GM giudizio o un tipo specifico di impostazione.
    * Cannoneer
    * Esorcista
    * Negromante di Inventor
    * Goblin
    * Molti Goblin
    * Paladino della Parola

# Extra
Le classi extra non possono essere prese come primo modello. Sono strani, oscuro e strano. Adattali se qualcuno vuole livellare come a Vampiro o qualcosa del genere.
    * Surly Gnome
    * Cannibal Gourmet

# Modelli e Multiclassing

Ogni classe ha 4 modelli. I modelli vengono acquisiti in sequenza (A, quindi B, quindi C, quindi D). Per multiclasse, scegli semplicemente il più basso modello di un'altra classe. Per esempio. Un personaggio di livello 4 potrebbe scegliere
Combattente A, Mago A, Monaco A e Combattente B.
Il multiclassing dovrebbe avere senso. Multiclass per l'ottimizzazione o ragioni meccaniche dovrebbero essere fortemente scoraggiate dal GM. Il gioco non riguarda la meccanica o il più grande numeri. Numeri più grandi non ti salveranno.

# Salire di livello
I PC guadagnano punti esperienza (XP) saccheggiando oggetti. I salari non lo fanno contare. Il bottino deve essere portato in un luogo sicuro e diviso o assegnato ai PC per diventare XP. Articoli o attrezzature utilizzati da i personaggi e non venduti non contano ai fini di XP. Giocatori dovrebbe tracciare l'XP di un PC (la quantità totale di tesori che possiede accumulato) insieme al loro denaro corrente. Puramente frivolo la spesa converte il 10% del denaro speso in XP.

Ogni volta che un PC passa di livello, aumenta i suoi HP, Attack e Base save (prima che venga aggiunto il loro bonus di Carisma). Un PC può anche testare migliorare una statistica di loro scelta. Dichiara lo stat e lancia 3d6. Se la il risultato è finito, il valore della stat aumenta di 1. Al livello 5, e ogni volta che un PC passa al livello 5, possono farlo ritirarsi in sicurezza. Il GM non può più tormentarli. Se essi possono permetterselo, possono comprare un po' di terra, aprire un negozio, insegnare a mago college, o implorare nella grondaia. A livello 10 e oltre, a

Il PC che muore può morire. Se ci riescono, possono tornare a vita. Tutti i futuri salvataggi per evitare la morte saranno puniti, ma loro ottenere una seconda possibilità.

# Generazione di personaggi

| 1d10 Classi di base | 1d6 Classi di novità |
| ------------------- | ---------------------|
| 1 caccia | 1 cannoneer |
| 2 Barbarian | 2 Exorcist |
| 3 Knight | 3 Inventor Necromancer |
| 4 ladro | 4 Goblin |
| 5 Hunter | 5 Molti Goblin |
| 6 Monaco | 6 Paladino della Parola |
| 7 Wizard | |
| 8 stregone | |
| 9 Summoner | |
| 10 [rilancio] o novità | |

| 1d12 | Scuole di magia |
| ---- | -------------- |
| 1 | animatore (fuorilegge) | 
| 2 | Biomancer (Outlawed) |
| 3 | Maledizione-Mangiatore (Fuorilegge) |
| 4 | Drowned (Outsider) |
| 5 | Elementalist (Chartered) |
| 6 | Elfo (esterno) o Garden (Chartered) |
| 7 | geometro (noleggiato) |
| 8 | Illusionist (Chartered) |
| 9 | Negromante (Fuorilegge) |
| 10 | Orthodox (Chartered) |
| 11 | Spider (Outsider) |
| 12 | White Hand (Chartered) |

| Livello | XP | HP (20 max) | Modelli di classe | Attacco Base | Tiro Salvezza |
| ------- | -- | ----------- | ----------------- | ------------ | ------------- |
| 1 | - | Con - 4 | 1 | 11 | 6 |
| 2 | 200 | Con - 2 | 2 | 12 | 7 |
| 3 | 400 | Con | 3 | 12 | 7 |
| 4 | 700 | Con + 2 | 4 | 13 | 7 |
| 5 | 1.000 | Con + 4 | - | 13 | 8 |
| 6 | 1.400 | Con + 6 | - | 14 | 8 |
| 7 | 1.800 | Con + 7 | - | 14 | 8 |
| 8 | 2.200 | Con + 8 | - | 15 | 9 |
| 9 | 2.600 | Con + 9 | - | 15 | 9 |
| 10 | 3.000 | Con + 10 | - | 15 | 10 |
| +1 | +500 | +1 | - | 15 | 10 |

| Stat | Bonus |
| 1,2 | -3 |
| 3,4,5 | -2 |
| 6,7,8 | -1 |
| 9,10,11 | 0 |
| 12,13,14 | 1 |
| 15,16,17 | 2 |
| 18,19,20 | 3 |
| 21,22,23 | 4 |
| 24+ | 5 |
