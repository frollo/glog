Procedure sotterranee
Il monitoraggio del tempo è fondamentale. Uso minuti e ore.
Movimento
I PC possono spostare cautamente 20 quadrati da 10 "(200") in 10 minuti.
Arrotondare ai 10 minuti più vicini. I PC si muovono con cautela
segni spot di tutte le trappole. La parte divertente non sta notando la trappola, è come
per disarmarlo. Nessuna abilità. Usa il buon senso e gli articoli di inventario.
I PC possono spostare rapidamente 60 quadrati da 10 "(600") in 10 minuti. PC
muoversi incautamente può avere la possibilità di passare sotto Saggezza a
notare una trappola (discrezione di GM). Altrimenti, intrappolano tramite HP
e mercenari.
Se si desidera modificare questi tassi di movimento in base a
Movimento, usa 20 "+ il movimento più basso nel gruppo per
movimento prudente e 3 volte quel valore per un movimento rapido. In
pratica, raramente sembra importare.
ricerca
• Una ricerca rapida di una stanza 20'x20 'dura 1 minuto e
rivela solo le informazioni più ovvie.
• Una ricerca corretta richiede 30 minuti e rivela la maggior parte nascosta
informazione.
• Una ricerca dettagliata richiede 1 ora e rivela tutto nascosto
informazione.
• Più PC possono combinare i loro sforzi per effettuare ricerche più grandi
stanza nello stesso tempo o una stanza più piccola in meno tempo.
Incontri casuali
Tira per un incontro casuale ogni 30 minuti o ogni volta a
si verifica un forte rumore. Di solito, questo è un 1 su un d6. Alcune persone usano
un dado sovraccarico / pericoloso, dove i risultati di 2-6 fanno qualsiasi cosa
dal controllo delle fonti luminose al fornire indizi. Non mi preoccupo.
Il monitoraggio del tempo funziona altrettanto bene.
Se i PC si muovono con cautela, ottengono l'Omen prima del
incontrare. Se si muovono rapidamente o incautamente, non lo fanno
prendi l'Omen.
Rotolo 2d6 per Morale. Se il risultato è uguale o inferiore al morale, il
la creatura è ostile. Se sopra, positivo o incerto. Modifica morale
da qualcosa da +4 a -4 a seconda delle circostanze.
Leggero
Rivedi le regole della luce. Le lanterne illuminano 30 'in modo chiaro e chiaro
fornire contorni e ombre tenui per altri 30 '. Nell'oscurità,
le sorgenti luminose sono visibili a miglia di distanza.
Altre attività
• La selezione di un blocco banale richiede 10 minuti.
• L'identificazione di un oggetto magico richiede 10 minuti.
• Decifrare un'iscrizione, leggere un libro o ordinare
attraverso gli articoli richiede 30 minuti.
• Il pranzo dura 1 ora. Prova due volte incontri casuali.
• Ci sono circa 6 round di combattimento in 1 minuto. Non ti preoccupare
sul numero esatto di round. Supponi solo il combattimento, a
poi respirare e il saccheggio dura circa 10 minuti.
• Bulbo oculare di altre attività basate sull'esperienza della vita reale.
Arrotondare un po '.
CC BY-NC-SA
6
Dungeon & Wilderness Procedures
Procedure di deserto
Gli esagoni hanno una durata di 6 ore. Non miglia, ore. Se sono necessarie miglia,
i PC possono viaggiare per 30 miglia al giorno. I cavalli non permettono ai PC
viaggiare più rapidamente, ma rendono il viaggio più semplice.
Ogni esagono viaggia a piedi con 1 slot di inventario con fatica.
Cavalcare un cavallo o un carro annulla il guadagno della fatica a meno che il
il tempo è terribile o la strada è molto accidentata.
Gli esagoni contengono una caratteristica ovvia (qualcosa che i PC faranno
incontro) e una funzione nascosta (qualcosa che i PC potrebbero
incontrarsi se cercano l'esagono, trascorrono del tempo lì o visitano
più volte).
Test per un incontro casuale ogni 6 ore o ogni volta che i PC
fare molto rumore o fare qualcosa che potrebbe attirare
Attenzione. Di solito, questo è un 1 su un d6.
Rotola per il tempo ogni giorno.
La maggior parte delle attività richiede 1 ora.
I PC possono viaggiare in sicurezza per 12 ore al giorno (2 esagoni) o spingere
la loro fortuna con 18 ore (3 esagoni), perdendo i benefici del riposo
e pranzare.
Nelle parti civili, villaggi e contadini sono ovunque. In
aree selvagge, strade potrebbero non esistere. Tutta la terra è di proprietà, anche
rifiuti senza traccia.
 
