Combattere
Passa ai round di combattimento quando sembra che qualcuno potrebbe ottenere
male.
Se qualcuno (o un gruppo) attacca di sorpresa, possono agire
prima che vengano lanciate le iniziative per il round. Una volta tutto
le imboscate hanno agito, tutti tirano per l'iniziativa.
Iniziativa
Ad ogni round di combattimento, un PC deve rotolare sotto la sua Saggezza. Se
ci riescono, agiscono davanti ai loro nemici. Se falliscono, agiscono
successivamente. Agiscono in ordine di lancio, dal più basso al più alto.
Alcuni nemici particolarmente veloci potrebbero imporre una penalità
Saggezza dei PG, o agire due volte in ogni round.
Al loro turno
Un PC o un nemico possono effettuare una delle seguenti operazioni:
• Esegui un tiro d'attacco contro un bersaglio.
• Corri per circa 50 "+ movimento (o 60").
• Effettua un attacco di carica contro un bersaglio (Sposta
20 '+ movimento e poi effettua un tiro di attacco).
• Lancia un incantesimo o attiva un oggetto magico.
• Recupera un oggetto da uno slot non di rinvio.
• Ricarica una balestra, ecc.
• Prova a inciampare, spingere o spingere un bersaglio.
• Tentativo di rimuovere una ferita fatale da un alleato morente.
Inoltre, un PC o un nemico possono muovere 20 '+ movimento (o 30').
Parlare, gridare o far cadere un oggetto può essere fatto liberamente, e
forse anche a turno di altre persone. Fondamentalmente, puoi farne uno
grande cosa a turno, oltre a muoversi un po '.
Tiri d'attacco
Tira sotto Attacco, opposto dalla Difesa del nemico, per colpire.
Per esempio. Un Ladro di 1 ° livello con Attacco 11 tenta di colpire un folletto con no
armatura. Il ladro deve passare sotto 11.
Un ladro di 1 ° livello con Attacco 11 tenta di colpire una tigre molto agile
(armatura come catena). Il ladro deve rotolare sotto 7 (11+ [10-14]).
Altri effetti (ubriachezza, nebbia, presenza di calore, ecc.) Possono imporre
penalità all'attacco.
Le armi a distanza ottengono -1 in attacco per ogni 10 "oltre il loro
intervallo elencato.
La maggior parte degli incantesimi non richiede un tiro di attacco. Se un incantesimo richiede
uno, verrà annotato nella descrizione dell'incantesimo. Toccando un
un bersaglio riluttante richiede sempre un tiro d'attacco.
Successi e insuccessi critici
Con un tiro per Attacco non modificato di 1, l'attacco infligge il doppio
danno (tira il doppio del numero di dadi e aggiungi eventuali bonus
due volte).
Con un tiro di Attacco non modificato di 20, l'attacco automaticamente
manca. Ulteriori sanzioni possono essere applicate se la situazione è rischiosa. UN
un fallimento critico in un combattimento uno contro uno in un prato limpido potrebbe essere giusto
essere una signorina. Un fallimento critico in un corridoio stretto, con alleati e
i nemici che si spingono insieme, potrebbero provocare la caduta di un alleato, a
l'arma viene lanciata o l'attaccante cade incline.
Danno
• Le armi leggere (pugnali) danno 1d6 + bonus alla forza
danno.
• Le armi medie (spade) danno 1d8 + bonus alla forza
danno se maneggiato in una mano o 1d10 + bonus alla forza
danno se maneggiato con entrambe le mani.
• Le armi pesanti (martelli) infliggono 1d12 + bonus alla forza
danno ma deve essere maneggiato con entrambe le mani.
• Le armi leggere lanciate infliggono 1d6 danni.
• Archi e fionde infliggono 1d6 danni.
• Le balestre infliggono 1d12 danni.
Punti ferita
Il pericolo a cui un personaggio può resistere prima di subire una ferita è
quantificato da Hit Points (HP). Sono meglio pensati come
Punti "Non farti colpire". Non ci sono effetti meccanici per l'essere
ridotto a 0 HP. Qualsiasi danno in eccesso (ovvero HP negativi)
è noto come Lethal Damage, che comporta sempre un tiro sul
Tavolo della morte e dello smembramento.
I PC hanno un massimo di HP di 20. I nemici hanno HP variabili
basarsi sui loro Hit Dice (HD). Un dado per colpire è un d8. Rotola il numero di
HD elencato e sommare i numeri. Un nemico con
HP sorprendentemente bassi potrebbero essere malati o feriti. Un nemico con
HP alti potrebbero essere un campione (o due nemici).
Guarigione
1. Il pranzo cura un personaggio per 1d6 + HP di livello. Il pranzo richiede 1
ora, richiede un posto sicuro e consuma 1 razione.
2. Un buon riposo notturno. Ripristina tutti gli HP. Richiede 8 ore di
riposo, un posto sicuro, una fonte di calore o di calore e consuma 1 razione.
3. Guarigione magica. Alcuni incantesimi o pozioni ripristinano HP. Altri
può guarire le lesioni.
Se un personaggio ha HP negativi (cioè ha subito danni letali),
il prossimo pranzo o il resto della buona notte li guarisce a 0 e non
oltre 0.
Protezione delle armi
Tutte le classi sono dotate di pugnali. Anche un PC è utile
con qualsiasi arma che ottengono da una classe o da un background. Un PC
ha -4 per attaccare con un'arma fino a quando non ottengono competenza
atterrando 8 colpi riusciti in combattimento.
Rotoli di reazione e morale
Quando incontri dei mostri, tira 2d6 + il bonus di Carisma di
il membro del partito più visibile.
I mostri (o mercenari) possono tentare di scappare o arrendersi se
il combattimento si trasforma contro di loro. I mostri hanno un valore morale elencato
da 2 (craven) a 12 (infrangibile). Controlla il morale quando:
1. Il lato (PC o mostri) prende la prima morte.
2. Quando metà del lato è stato inabilitato o ucciso.
3. Se si verifica un effetto particolarmente spaventoso o spettacolare.
Rotolo 2d6. Se il risultato supera il punteggio di Morale delle creature, il
la creatura tenta di ritirarsi, arrendersi o panico. Il morale può
essere aggiustato (salvo 2 o 12) con bonus situazionali