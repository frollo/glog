
Limite di caratteri: 5000
Ci sono elementi del caso.
Hai mai giocato a poliziotti e ladri o altri giochi inventati?
Hai mai incontrato un bambino che ha detto "Nuh, non hai colpito
io, ho un giubbotto antiproiettile! "In questo gioco di finzione,
ci sono alcune regole per aiutare a decidere chi colpisce chi e quanto
e altre cose del genere.
I tuoi personaggi avranno anche dei numeri casuali
assegnato a loro. A volte i numeri saranno buoni, e
a volte saranno cattivi e non c'è molto che puoi fare
a proposito. In questo gioco i numeri non sono così importanti.
A volte, un personaggio ha statistiche terribili e sopravvive per anni,
mentre un personaggio con statistiche brillanti muore nella prima ora di gioco.
Ci sono elementi di abilità.
In Settlers of Catan, le regole sono rigide, solide ed equilibrate.
Non puoi andare "Ehi Steve, prenderò i miei cavalieri e tu prenderai i tuoi
Knights e andremo a saccheggiare quella piastrella di pecora. "Non è nel
regole. E questo è uno. Ma in un gioco come questo, puoi venire
con qualsiasi piano tu voglia. In questo modo il gioco è come un vero
mondo e le persone in esso sono come persone reali. A nessuno importa
troppo se vai in giro a distruggere ceramiche e saccheggiare case
in un videogioco, ma in questo gioco, quel genere di cose ha qualcosa di reale
conseguenze. Allo stesso modo, non puoi attirare Bowser dal suo castello
con un delizioso pasto nel videogioco perché cucinare no
codificato nel gioco.
Elaborare un buon piano ed eseguirlo è
importante. Essere intelligenti. Fai attenzione. Pensa a soluzioni che il
le regole non coprono.
Nel personaggio contro fuori dal personaggio
Non devi fare voci sciocche o indossare costumi. Lo assicurerai
come ti senti a tuo agio come personaggio mentre andiamo
lungo. La cosa principale, in questo momento, è ricordare che il tuo
il personaggio conosce cose che non conosci e che conosci
cose che il tuo personaggio non sa.
Il tuo personaggio conosce cose del mondo che tu, come a
giocatore, non lo so. Sanno come cavalcare o fare un cavallo
avvelenare o identificare rune magiche. Se c'è una situazione in cui
si applica, ti dirò solo quello che sa il tuo personaggio. Se non lo sono
certo, potrei farti andare per questo.
Allo stesso modo, ci sono cose che sai che i tuoi personaggi non sanno
conoscere. Il principale è la ragione. Viviamo in un'era logica; il
i personaggi che stai interpretando no. Non hanno il vantaggio di a
educazione post-illuministica. Potrebbero anche non essere alfabetizzati.
Non hai paura del tempo perché capisci cosa
il tempo è. I tuoi personaggi no. Potrebbero non capire
cose che diamo per scontate, e potrebbero agire per motivi
che saremmo stati arcaici o ridicoli.
Tu, il giocatore, potresti sapere cose su altri personaggi o sul
mondo che il tuo personaggio non ha. Potresti conoscere un segreto
il passato di un altro personaggio, o il modo migliore per uccidere un vampiro. Ma
ricorda, il tuo personaggio può agire solo sulla conoscenza che ha
nel gioco. Ne discuteremo di più man mano che si presenta.
I tuoi personaggi moriranno.
Hai mai giocato a Super Mario Bros? Mario è morto molto in questo
gioco. A volte è morto perché hai fatto un errore.
A volte era un caso, o fortuna, o eri distratto, o tu
non capivo come funzionava un nemico. Questo gioco è così,
tranne che nel videogioco, Mario ritorna ancora e ancora. In
questo gioco, ogni volta che ottieni un nuovo personaggio.
Far morire un personaggio non significa che tu abbia commesso un errore o
fallito. Fa parte del processo di apprendimento. A volte è giusto
possibilità casuale.
Devi creare un nuovo personaggio, e io troverò un modo per presentare
loro al gruppo. Inizi di nuovo dal livello 1 ma non è un grande
affare. Un personaggio di livello 1 è utile quanto un personaggio di livello 4
quasi ogni modo. Non sarai mai completamente surclassato.
Livellamento
Sali di livello e migliora il tuo personaggio saccheggiando il tesoro.
Immagina un drago e il suo tesoro. Uccidi il drago e ottieni 0
XP. Ruba il tesoro, con la forza o la furtività, e conta. voi
devi tirarlo fuori dalla prigione e portarlo in un posto sicuro,
ma una volta che è sicuro - sepolto, speso, investito o donato - conta.
Il contratto sociale
Se ti piace questo gioco e ti stai divertendo, mostrati in tempo o
dimmi in anticipo se non ce la fai. Se non ti piace questo gioco,
fammi sapere e faremo cambiamenti. Semmai di questo
il gioco ti mette a disagio, allevalo immediatamente. Se tu
non sentirti a tuo agio nel farlo, o ti verrà in mente solo in seguito,
puoi sempre inviarmi un'e-mail.
Per favore non tirare fuori i telefoni durante il gioco a meno che non ci sia
qualcosa di urgente. Idealmente questa è la cosa più interessante da fare
su. Allo stesso modo, capisci che ci sono altre 5 persone
la stanza, quindi non perdere deliberatamente tempo.
L'impostazione di questo gioco è semi-medievale. Ciò significa che
cose terribili accadono regolarmente. Sembrerebbe di esserlo
imbiancare o disney�ng il passato se dicessi che questa impostazione
non include cose come misoginia, razzismo, violenza sessuale,
ecc. Queste cose accadono nel mondo reale. Sarebbe strano
avere un mondo d'azione senza di loro. Detto questo, non lo faranno
formano una parte importante di questo gioco.
Gender è anche molto più importante in questo gioco di quanto non lo sia in
mondo moderno, ma forse meno importante di quanto si possa temere. A
la parte più alta della società, puoi cavartela con la rottura
norme di genere per ricchezza, potere, legge e abilità. Al minimo
fine, a nessuno importa. Sono solo le classi medie e la Chiesa
che sono ossessionati dalla proprietà e dalla stazione. Probabilmente non sarà un
parte importante della maggior parte delle sessioni.
Le regole
Sono sempre disponibili per la lettura. Non sono segreti. Se tu
E incoerenze o errori, fammi sapere, ma le regole non lo sono
la legge. Come GM, ciò che dico va e, se necessario, ciò che dico,
andato. Se pensi di aver trovato una scappatoia, ma i risultati di ciò
la scappatoia non ha alcun senso, smetterà di funzionare immediatamente.
Domande
Se hai domande, fammi sapere. Alcune domande di esempio
includere:
"Posso colpirlo con una freccia a questa distanza?"
"Che sapore ha X?"
"Il mio personaggio sa qualcosa di X?"
"Quanto danno fa una spada lunga se la lancio
qualcuno?"
Prima di iniziare, hai domande in questo momento?
